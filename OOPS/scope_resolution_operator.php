<?php
class A{
   const PI=3.142;
   static $x=10;
}
//outside a class
echo A::PI."<br>";
echo A::$x."<br>";
$var='A';
echo $var::PI."<br>";
echo $var::$x."<br>";
?>
<?php
//inside a class using (::)
class Demo{
    const PI=3.142;
    static $x=10;
    static function show(){
       echo self::PI."<br>";
       echo self::$x."<br>";
    }
 }
 Demo::show();
 ?>

 <?php
 //in override a class we can call the parent method using parent keyword
 class Base
 {
     function override() {
         echo"Parent class<br>";
     }
 }
 class child extends Base
 {
     function override() {
         parent::override();   //calling the parent class method
         echo"Child class";
     }
 }
 $obj1=new child();
 $obj1->override();
 ?>