<?php
$var1=10;
$var2=&$var1;
echo "$var1 $var2<br/>";
$var2=20;
echo "$var1 $var2<br/>";
?>



<?php
$arr=array(1,2,3,4,5,6);
$i=&$ref;
foreach($arr as $i)
echo $i*$i, "<br/>";
echo "ref = ". $ref;
?>

<?php
//passing by referencce
function helo(&$val){
    $val--;
    echo"$val";
}
$a=3;
helo($a);

function hi(&$val){
    $val++;
    echo "$val<br/>";
}
function say(){
    $a=5;
    return $a;
}
hi(say());
?>
<?php
//returning reference
function &myfunc(){
	static $x=10;
	echo " Inside function: ",$x,"<br/>";
	return $x;
}
$a=&myfunc(); 
echo "returned by reference: ", $a, "<br/>";
$a=$a+10; 
$a=&myfunc();

class myclass{
    public $value=10;
    function &gettingvalue(){
        return $this->value;
    }
}
$obj=new myclass();
$myvalue=&$obj->gettingvalue();
echo "$myvalue <br/>";
$obj->value=2;    //assigning new value
echo "$myvalue<br/>";
?>

<?php
//unsetting references
$a1=10;
$b1=&$a1;
echo "Reference variable :$b1<br/>";
unset($b1);
$b1=10;
echo "After unsetting the reference variable :$b1<br/>";

$a2=20;
$b2=&$a2;
echo"$b2<br/>";
$b2=null;
echo "After assign a null:$b2<br/>"
?>

<?php
//spotting reference
$val1 = 'Hello World';
function myfunction(){
   global $val1;
   $val2 =&$val1;
   echo "$val1, $val2 <br/>";
   $val2="Hello PHP";
   echo "$val1, $val2 <br/>";
   unset($val1);
}
myfunction();
echo "$val1\n";
?>
