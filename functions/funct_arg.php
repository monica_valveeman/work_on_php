<?php
function func1($type1, $type2 = "World")
{
    echo $type1 . $type2 . "<br/>";
}
func1("Hello");
?>
<?php
function sum() {
    $acc = 0;
    foreach (func_get_args() as $n) {
        $acc += $n;
    }
    return $acc;
}

echo sum(1, 2, 3, 4);
?>

<?php
function add($x, $y){
   $x= $x+$y ;
   echo "<br/>".$x . "<br/>";
}
$x=10;
$y=20;
add($x,$y);
//outside function $x has previous value.
echo $x;
?>

<?php
//passing array to function
function arrpass($arr){
   $sum=0;
   foreach ($arr as $i){
      $sum+=$i;
   }
   echo "<br/>sum = " .$sum;
}
arrpass(array(1,2,3));
?>