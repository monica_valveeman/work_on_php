<?php
$str1=array('Have','a','nice','day');
echo implode(" ",$str1)."<br>";
echo implode("+",$str1)."<br>";
echo implode("-",$str1)."<br>";
echo implode("*",$str1)."<br>";
//without a separator
echo implode($str1)."<br>";
?>

<?php
   $a1 = array("1","2","3");
   $a2 = array("a");
   $a3 = array();
   
   echo "a1 is: '".implode("','",$a1)."'<br>";
   echo "a2 is: '".implode("','",$a2)."'<br>";
   echo "a3 is: '".implode("','",$a3)."'<br>";
?>

<?php
$str2=array('Have','a','wonderful','day','!!!!');
echo join(" ",$str2)."<br>";
echo join(",",$str2)."<br>";
?>