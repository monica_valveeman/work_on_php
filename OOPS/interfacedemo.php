<?php
//constant using interface
interface A
{
    const Constant = 'Interface constant';
}

echo A::Constant."<br>";
?>

<?php
//interface implements demo
interface demo
{
    public function personaldetails($detail);
}

class fname implements demo
{
    public $fname;
    public function personaldetails($fname) {
        $this->fname=$fname;
        echo "Fname : {$this->fname}<br>";
    }
}
class lname implements demo
{
    public $lname;
    public function personaldetails($lname) {
        $this->lname=$lname;
        echo "Lname : {$this->lname}<br>";
    }
}
$fname=new fname();
$fname->personaldetails("Monica");

$lname=new lname();
$lname->personaldetails("Valveeman");
?>