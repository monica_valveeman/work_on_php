<?php
//syntax
define('firstName','Monica');
define('lastname','Valveeman');
define('age',21);
?>

<?php
//defining constants
define('CONSTANT','Hello world');
echo CONSTANT;
//using const keyword
const name="Monica";
echo name;
?> 

<?php
//magic constants
echo "You are in".__LINE__ ."line";
echo __FILE__;
echo __DIR__;
?>
