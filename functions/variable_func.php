<?php
function hello()
{
   echo "Hello World";
}
$var="Hello";
$var();
?>

<?php
//with arguments
function add($a, $b)
{
    return $a * $b;
}
$c = "add";
echo $c(10,5);
?>

<?php
//using class methods
class myclass{
   function welcome($name){
      echo "Welcome $name";
   }
}
$obj=new myclass();
$f="welcome";
$obj->$f("Monica");
?>