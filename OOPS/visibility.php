<?php
class Demo
{
    public $name = "Monica";
    protected $age = 21;
    private $rno = 51;

    function display() {
        echo"$this->name<br>";
        echo "$this->age<br>";
        echo "$this->rno<br>";
    }
}
$obj=new Demo();
$obj->display();
echo "$obj->name<br>";
//echo "$obj->age<br>";       //error
//echo "$obj->rno<br>";       //error
?>

<?php
class Demo1
{
    public $str="Welcome to visibility methods";
    public function Mypublic() {
        echo"Public visibility method<br>";
    }
    private function Myprivate() {
        echo"Private visibility method<br>";
    }
    protected function Myprotected() {
        echo"Protected visibility method<br>";
    }
    function displaymethods() {
        $this->Mypublic();
        $this->Myprotected();
        $this->Myprivate();
    }
}
$object=new Demo1();
$object->displaymethods();
$object->Mypublic();
//$object->Myprotected();
//$object->Myprivate();
?>