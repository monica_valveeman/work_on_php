<?php
//array_reverse
$rev=array("a"=>"Volvo","b"=>"BMW","c"=>"Toyota");
print_r(array_reverse($rev));
echo"<br>";
?>

<?php
//search
$a=array("a"=>"red","b"=>"green","c"=>"blue");
echo array_search("red",$a);
echo"<br>";
?>

<?php
//slice
$a=array("red","green","blue","yellow","brown");
print_r(array_slice($a,1,2));
echo"<br>";
print_r(array_slice($a,-2,1));
echo"<br>";
?>
<?php
//sum
$a=array("a"=>52.2,"b"=>13.7,"c"=>0.9);
echo array_sum($a)."<br>";
?>

<?php
//unique- removes the duplicate values
$a=array("a"=>"red","b"=>"green","c"=>"red");
print_r(array_unique($a));
echo"<br>";
?>

<?php
//array_values(arr);
$a=array("Name"=>"Monica","Age"=>"21","Country"=>"India");
print_r(array_values($a));
echo"<br>";
?>

<?php
//array_walk(arr,function,parameter...);
function myfunction($value,$key,$p)
{
echo "$key $p $value<br>";
}
$a=array("a"=>"red","b"=>"green","c"=>"blue");
array_walk($a,"myfunction","has the value");
?>
<?php
//sorts
$age=array("Peter"=>"35","Ben"=>"37","Joe"=>"43");
//ascending
asort($age);
foreach ($age as $key => $val) {
    echo "$key = $val<br>";
}
//descending
arsort($age);
foreach ($age as $key => $val) {
    echo "$key = $val<br>";
}
?>

<?php
//compact- create a array from the variables
$firstname = "Monica";
$lastname = "Valveeman";
$age = "21";

$result = compact("firstname", "lastname", "age");

print_r($result);
echo"<br>";
?>

<?php
$a=array("red","green","blue","yellow","brown");
echo current($a)."<br>";
print_r(each($a));
echo"<br>";
echo next($a)."<br>";
print_r(each($a));
echo"<br>";
echo current($a)."<br>";
echo prev($a)."<br>";
echo end($a)."<br>";;
echo reset($a)."<br>";
?>

<?php
//extract
$a = "Original";
$my_array = array("a" => "Cat","b" => "Dog", "c" => "Horse");
extract($my_array);
echo "\$a = $a; \$b = $b; \$c = $c";
echo"<br>"
?>

<?php
//list-used to assign values to a list of variables in one operation
$my_array = array("Dog","Cat","Horse");

list($a, $b, $c) = $my_array;
echo "I have several animals, a $a, a $b and a $c.";
echo"<br>";
?>

<?php
//range(low,high,step)
$number = range(0,50,10);
print_r ($number);
echo"<br>";
$letter = range("a","d");
print_r ($letter);
echo"<br>"
?>
<?php
//shuffle(arr);
$arr= array("red","green","blue","yellow","purple");
shuffle($arr);
print_r($arr);
echo"<br>";
?>
