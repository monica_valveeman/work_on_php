<?php

//stripos(str,find,start)
//first occurence
echo stripos("Hello","l",2);
echo stripos("Hello","l",3);
?>

<?php
//case-insensitive
echo stripos("GOOD","o");

?>
<?php
//also calculates the whitespace
echo stripos(" String methods","string");
?>