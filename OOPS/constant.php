<?php
class MyClass
{
    const constantvalue = 'constantvalue';

    function democonstant() {
        echo  self::constantvalue . "<br>";
    }
}

echo MyClass::constantvalue . "<br>";

$classname = "MyClass";
echo $classname::constantvalue . "<br>";

$class = new MyClass();
$class->democonstant();

echo $class::constantvalue."<br>";
?>

<?php

//constants an expressionns
class constdemo
{
    const length=10;
    const breadth=25;
    function rectangle(){
        //inside class
        echo "Area = ".self::length * self::breadth;
        echo"<br>";
    }
}
$obj=new constdemo();
$obj->rectangle();
//outside class
echo "Area =".$obj::length * $obj::breadth;
?>