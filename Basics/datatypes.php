<?php
$fname="Monica";
$lname='Valveeman';
$age=21;
$bool=true;

echo gettype($fname);
echo gettype($lname);
echo gettype($age);
echo gettype($bool);

if(is_int($age)){
    echo'This is an integer';
}
if(!(is_string($bool))){
    echo'Not a bool';
}

?>


<?php
var_dump(25/7);         // float(3.5714285714286)
var_dump((int) (25/7)); // int(3)
var_dump(round(25/7));  // float(4)
?>

<?php
$a = 1.23456789;
$b = 1.23456780;
$epsilon = 0.00001;

if(abs($a-$b) < $epsilon) {
    echo "true";
}
?>
<?php 
//array - Typecasting and overwritten
$array=array(1=>"a", "1"=>"b",1.5=>"c", true=>"d");
var_dump($array);

//mixed int and string keys
$arr=array("hi"=>"hello","hello"=>"hi",90=>-90,-90=>90);
var_dump($arr);

//indexed arrays without keys
$aa=array("hi","hello",1,2);
var_dump($aa);

//keys not all elements
$aa1=array("A","B","C",5=>"D","E");
var_dump($aa1);

//using unset() function
$aa2=array(3=>"a",14=>"b");

$aa2[]=90;
var_dump($aa2);
unset($aa2[14]);
var_dump($aa2);
//unset($aa2);
?>
<?php
//objects
class Hello{
    function hi(){
        echo"Hello!!!"; 
    }
}
$obj=new Hello();
$obj->hi();

//converting to object
$obj1 = (object) array(2 => 'Monica',3=>'Valveeman');
var_dump(isset($obj1->{3}));
var_dump(key($obj1));

$obj2 = (object) 'monica.valveeman';
echo $obj2->scalar;
?>
<?php
$varr=NULL;
var_dump($varr);
if($varr==null){
    echo'true';
}
if(is_null($varr)){
    echo'TRUE';
}
?>

<?php
//callbacks / callables
function mycallback(){
    echo"Hii<br/>";
}
class myclass{
    static function mycallmethod(){
        echo"hello<br/>";
     }
}
//simple callback
call_user_func('mycallback');

//static class method call
call_user_func(array('myclass','mycallmethod'));

call_user_func('myclass::mycallmethod');

$obj3=new myclass();
call_user_func(array($obj3,'mycallmethod'));
?>

<?php
function sum($a, $b)  {
    return (float) $a + $b;
}

// Note that a float will be returned.
var_dump(sum(1, 2));
?>