<?php    
    // original string   
    $str1 = "Hello, we are here to help you.";     
      
    // Passed zero   
    print_r (explode (" ",$str1, 0));  
    echo"<br>"; 
    // Passed positive value  
    print_r (explode (" ",$str1, 4)); 
    echo"<br>";  
    // Passed negative value   
    print_r (explode (" ",$str1, -3));     
    echo"<br>";
?>  

<?php    
    // original string   
    $str2 = "Hello, welcome to explode method.";     
    //without passing optional parameter  
    print_r (explode (" ", $str2));  
    echo"<br>";
?>  

<?php    
    
    $str3 = "Hello, welcome to explode method.";     

    print_r (explode ("e", $str3));  
?>  