<?php   
//print the variable value
$str ="Hello echo method";  
echo "By using 'echo()' function your string is :".$str;   
?>  
<?php   
//using html tags
$str1 ="Hello echo ";  
$str2 ="method";  
echo "By using 'echo()' function your string is :".$str1."<br>".$str2 ;   
?>  
<?php  
//prints the value of expressions
echo "Your number value is: (1 + 2)"."<br>";  
echo "By using 'echo()' function: Addition value is: ".(1 + 2)."<br>";  
?>  

<?php  
//use a multiple parameters
echo "Your string is : 'This ','string ','was ','made ','with multiple parameters.'"."<br>";  
echo 'This ','string ','was ','made ','with multiple parameters.';  
?>   

<?php
//prints the array values
$age=array("Monica"=>"21");
echo "Peter is " . $age['Monica'] . " years old.";
?>