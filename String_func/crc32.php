<?php
$str = crc32("Hello World!");
printf("%u<br>",$str);
echo $str;                         //equal
?>  

<?php
$checksum = crc32("The quick brown fox jumped over the lazy dog.");
printf("%u<br>", $checksum);
?>

<?php
$str1 = crc32("Hello world.");
echo 'Without %u: '.$str1."<br>";
echo 'With %u: ';
printf("%u",$str1);                     //not equal
?>