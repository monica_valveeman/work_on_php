<?php
namespace mynamespace;
function hello() {
   echo "Hello World<br>";
}
use mynamespace;
mynamespace\hello();
?>

<?php
//sub-namespace
namespace mynamespace2\mynamespace3;
function hello() {
   echo "Hello World<br>";
}
namespace mynamespace2\mynamespace4;
function hello(){
   echo "Hello World<br>";
}
use mynamespace2\mynamespace3;
hello();
use mynamespace2\mynamespace4;
hello();
?>

<?php
namespace namespacename;
class classname
{
    function __construct()
    {
        echo __METHOD__,"<br>";
    }
}
function funcname()
{
    echo __FUNCTION__,"<br>";
}
const constname = "namespaced";


$a = '\namespacename\classname';
$obj = new $a;                                     
$a = 'namespacename\classname';
$obj = new $a;                                    
$b = 'namespacename\funcname';
$b();                                             
$b = '\namespacename\funcname';
$b();                                           
echo constant('\namespacename\constname'), "<br>";      
echo constant('namespacename\constname'), "<br>";       
?>


<?php
//namespaced code
namespace MyPro;
echo  __NAMESPACE__;  
?>


