<?php
//inheritance
class Details 
{
    public $name;
    public $rno;
    function __construct($name,$rno) {
        $this->name = $name;
        $this->rno = $rno;
    }
    function introdetails() {
        echo "Name: $this->name<br>";
        echo "Rno: $this->rno<br>"; 
    }
}
class subjects extends Details
{
    function marks() {
        echo "The subjects are React,Php, Mysql";
    }
}
$object=new subjects("Monica",51);
$object->introdetails();
$object->marks();
?>

<?php
class A 
{
    protected function message(){
        echo"Base class protected method<br>";
    }
} 
class B extends A 
{
    function display() {
        echo"Public method derived class";
        $this->message();
    }
   
}
$obj=new B();
$obj->display();
?>

<?php
//override a method
class Hello
{
    function intro() {
        echo"Base class";
    }
}
class Hi extends Hello
{
    function intro() {
        echo"Child class";
    }
}
$obj=new Hi();
$obj->intro();
?>