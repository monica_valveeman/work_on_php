<?php
$str1=addcslashes('Hello, Monica','M');
echo $str1."<br/>";
?>
<?php
echo addcslashes('foo[ ]', 'A..z'). "<br/>";

?>

<?php
//Add backslash in certain character in string
$str2 = "Welcome to my string function examples!";
echo $str2;
echo"<br/>";
echo addcslashes($str2,'m')."<br>";
echo addcslashes($str2,'n')."<br>";
?>

<?php
//Add backslashes to a range of characters in a string
$str3 = "Welcome to my humble Homepage!";
echo $str3."<br>";
echo addcslashes($str3,'A..Z')."<br>";
echo addcslashes($str3,'a..z')."<br>";
echo addcslashes($str3,'a..g')."<br>";
echo addcslashes($str3,'A..z')."<br/>";
echo addcslashes($str3,'z..a')."<br/>";
?>