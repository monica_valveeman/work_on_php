<?php
//strchr(string,search,bool)
//default it returns a same value-false
echo strchr("Hello world","world")."<br>";

//if true it returns the first occurence of rest of strings
echo strchr("Hello world","world",true)."<br>";
?>

<?php
$str1="string methods";
$asc=ord('m');
echo"Ascii value of m is $asc<br>";
echo strchr($str1,$asc)."<br>";

echo strchr($str1,$asc,true)."<br>";
?>
