<?php
//strcmp(str1,str2);
$str1="Hello";
$str2="Hello";
echo strcmp($str1,$str2)."<br>";  //equal - 0
?>

<?php
//greater than string 2 not equal returns >0
$str3="good Morning";
$str4="good ";
echo strcasecmp($str3,$str4)."<br>";
?>

<?php
//less than str6 not equal return < 0
$str5="good";
$str6="good morning";
echo strcasecmp($str5,$str6)."<br>";
?>

<?php
//case-sensitive
$str7="Hello";
$str8="hello";
echo strcmp($str7,$str8)."<br>";
echo strcmp($str8,$str7);
?>
