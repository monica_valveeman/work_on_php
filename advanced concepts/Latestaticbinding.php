<?php
class Demo1{
   public static $name="Monica";
   public static function name(){
      echo "name of class :" . __CLASS__."<br>";
   }
   public static function getname(){
      self::name();
   }
}
class child1 extends Demo1{
   public static function name(){
       echo parent::name()."<br>";
      echo "name of class :" . __CLASS__."<br>";
   }
}
child1::getname();
child1::name();
?>


<?php
//Using static:: instead of self:: creates late binding at runtime
class Demo2{
   public static $name="Monica";
   public static function name(){
      echo "name of class :" . __CLASS__."<br>";
   }
   public static function getname(){
      static::name();
   }
}
class child2 extends Demo2{
   public static function name(){
    
      echo "name of class :" . __CLASS__."<br>";
   }
}
child2::getname();

?>
