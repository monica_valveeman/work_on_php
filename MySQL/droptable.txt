//drop a column

mysql> alter table student drop column m3;
Query OK, 0 rows affected (0.62 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> desc student;
+---------+--------------+------+-----+---------+-------+
| Field   | Type         | Null | Key | Default | Extra |
+---------+--------------+------+-----+---------+-------+
| id      | int(4)       | YES  |     | NULL    |       |
| name    | varchar(50)  | YES  |     | NULL    |       |
| m1      | int(3)       | YES  |     | NULL    |       |
| m2      | int(3)       | YES  |     | NULL    |       |
| address | varchar(150) | YES  |     | NULL    |       |
| city    | varchar(30)  | YES  |     | NULL    |       |
+---------+--------------+------+-----+---------+-------+
6 rows in set (0.10 sec)

//the data only deleted not a table structure

mysql> truncate anothertable
    -> ;
Query OK, 0 rows affected (0.20 sec)

mysql> select * from anothertable;
Empty set (0.00 sec)

//delete a whole data and structure

mysql> drop table anothertable;
Query OK, 0 rows affected (0.13 sec)

mysql> desc anothertable;
ERROR 1146 (42S02): Table 'mysample.anothertable' doesn't exist1