
<?php
class A
{
    function foo()
    {
        if (isset($this)) {
            echo '$this is defined (';
            echo get_class($this);
            echo ")\n";
        } else {
            echo "\$this is not defined.\n";
        }
    }
}

class B
{
    function bar()
    {
        A::foo();
    }
}

$a = new A();
$a->foo();

A::foo();

$b = new B();
$b->bar();

B::bar();
?>

<?php
//object assignment
class simple{

}
$instance=new simple();
$object=$instance;
$reference=& $instance;
var_dump($instance);
var_dump($object);
var_dump($reference);
$instance=null;
var_dump($instance);
var_dump($object);
var_dump($reference);

?>

<?php
//properties and methods
class Check{
    public $a="Property";
    public function a(){
        echo"Method";
    }
}
$obj=new Check();
echo $obj->a."<br>";
$obj->a();
?>

