<?php
//star half pyramid
function pattern($n)
{
    for ($i = 0; $i < $n; $i++)
    {
        for($j = 0; $j <= $i; $j++ )
        {
            echo "* ";
        }
        echo "<br>";
    }
}
    $n = 5;
    pattern($n);
    echo"<br><br>";
?>

<?php
//star half pyramid-mirrored
function pattern2($num)
{
for ($i = 0; $i < $num; $i++)
{
for($k = $num; $k > $i+1; $k-- )
{
echo "  ";
}
for($j = 0; $j <= $i; $j++ )
{
echo "* ";
}
echo "<br>";
}
}
$num = 5;
pattern2($num);
echo"<br><br>";
?>

<?php
//Star half pyramid - Inverted 
function pattern3($num)
{
for ($i = $num; $i > 0; $i--){
for($j = 0; $j < $i; $j++ )
{
echo "* ";
}
echo "<br>";
}
}
$num = 5;
pattern3($num);
echo"<br><br>";
?>


<?php
//star half pyramid-Inverted mirrored
function pattern4($num)
{
for ($i = $num; $i > 0; $i--)
{
for($k = $i; $k < $num; $k++ )
{
echo "  ";
}
for($j = 0; $j < $i; $j++ )
{
echo "* ";
}
echo "<br>";
}
}
$num = 5;
pattern4($num);
echo"<br><br>";
?>
