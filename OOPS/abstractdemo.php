<?php
abstract class Abstractparent
{
    public $sub;
    public function __construct($sub){
        $this->sub = $sub;
    }
    abstract public function subject();
}
class PHP extends Abstractparent
{
    public function subject() {
        echo "The subject is {$this->sub}.<br>";
    }
}
class Mysql extends Abstractparent
{
    public function subject() {
        echo "The subject is {$this->sub}.<br>";
    }
}
class React extends Abstractparent
{
    public function subject() {
        echo "The subject is {$this->sub}.<br>";
    }
}
$sub1=new PHP("PHP");
$sub1->subject();

$sub2=new Mysql("Mysql");
$sub2->subject();

$sub3=new React("React");
$sub3->subject();
?>