<?php
class Details {
  public $name;
  public $age;
//constructor
  function __construct($name,$age) {
    $this->name = $name; 
    $this->age = $age;
  }
  function get_name() {
    return $this->name;
  }
  function get_age() {
      return $this->age;
  }
}

$obj = new Details("Monica",21);
echo $obj->get_name();
echo $obj->get_age();
?>
 <?php
 
 class Destruct
 {
     public $fullname;
     function __construct($fullname) {
         $this->fullname = $fullname;
     }
     //destructor
     function __destruct() {
         echo "My name is {$this->fullname}";
     }
 }
 $obj=new Destruct("Monica Valveeman");
 ?>