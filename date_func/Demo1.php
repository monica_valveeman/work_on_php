<?php
//date()
date_default_timezone_set('UTC');

// Prints the day
echo date("l") . "<br>";

// Prints the day, date, month, year, time, AM or PM
echo date("l jS \of F Y h:i:s A") . "<br>";

// Use a constant in the format parameter
echo date(DATE_RFC822) . "<br>";

echo date('l - jS F,Y');
?>
<?php
echo date("F j, Y, g:i a")."<br>";                 
echo date("m.d.y")."<br>";                         
echo date("j, n, Y")."<br>";                       
echo date("Ymd")."<br>";                           
echo date('h-i-s, j-m-y, it is w Day')."<br>";     
echo date('\i\t \i\s \t\h\e jS \d\a\y.')."<br>";   
echo date("D M j G:i:s T Y")."<br>";               
echo date('H:m:s \m \i\s\ \m\o\n\t\h')."<br>";     
echo date("H:i:s")."<br>";                         
echo date("Y-m-d H:i:s")."<br>";  
?>

<?php
//time()
$t=time();
echo($t . "<br>");
echo(date("Y-m-d",$t));
echo"<br>";
?>
<?php
$nextWeek = time() + (7 * 24 * 60 * 60);             // 7 days; 24 hours; 60 mins; 60 secs
echo 'Now:       '. date('Y-m-d') ."<br>";
echo 'Next Week: '. date('Y-m-d', $nextWeek) ."<br>";
// or using strtotime():
echo 'Next Week: '. date('Y-m-d', strtotime('+1 week')) ."<br>";
?>

<?php
//date_Diff
$date1=date_create("2013-03-15");
$date2=date_create("2013-12-12");
$diff=date_diff($date1,$date2);
echo $diff->format('%R%a days')."<br>";
?>


<?php
//strtotime
echo date('Y-m-d',strtotime("now")) . "<br>";
echo date('Y-m-d',strtotime("3 October 2005")) . "<br>";
echo date('Y-m-d',strtotime("+5 hours")) . "<br>";
echo date('Y-m-d',strtotime("+1 week")) . "<br>";
echo date('Y-m-d',strtotime("+1 week 3 days 7 hours 5 seconds")) . "<br>";
echo date('Y-m-d',strtotime("next Monday")) . "<br>";
echo date('Y-m-d',strtotime("last Sunday"))."<br>";
?>

<?php
//checkdate
var_dump(checkdate(12,31,-400));
echo "<br>";
var_dump(checkdate(2,29,2003));
echo "<br>";
var_dump(checkdate(2,29,2004));
?>

