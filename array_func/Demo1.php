<?php
//array_change_key_case(string,case)
$pets=array("a"=>"Cat","B"=>"Dog","c"=>"Horse","b"=>"Bird");

print_r(array_change_key_case($pets,CASE_UPPER));
echo"<br>";
print_r(array_change_key_case($pets,CASE_LOWER));
echo"<br>";
?>
<?php
$pets1=array("a"=>"Cat","B"=>"Dog","c"=>"Horse","b"=>"Bird");
print_r(array_chunk($pets1,2,false)); //chunk numerically
echo"<br>";
print_r(array_chunk($pets1,2,true)); //preserves the keys
echo"<br>";
?>

<?php
//array_combine(keys,values);
$keys=array("a","b","c");
$values=array("cat","dog","horse");

print_r(array_combine($keys,$values));
echo"<br>";
?>
<?php
//array_diff(arr1,arr2,...)
$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
$a2=array("e"=>"red","f"=>"black","g"=>"purple");
$a3=array("a"=>"red","b"=>"black","h"=>"yellow");

$result=array_diff($a1,$a2,$a3);
print_r($result);
echo"<br>";
?>

<?php
//array_fill(index,no.of element,value);
$fill=array_fill(3,4,"blue");
print_r($fill);
echo"<br>";
?>


<?php
function odd($var)
{
    // returns whether the input integer is odd
    return $var & 1;
}

function even($var)
{
    // returns whether the input integer is even
    return !($var & 1);
}

$array1 = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
$array2 = array(6, 7, 8, 9, 10, 11, 12);

echo "Odd :<br>";
print_r(array_filter($array1, "odd"));
echo"<br>";
echo "Even:<br>";
print_r(array_filter($array2, "even"));
?>
