<?php  
$str1 = "This is string method";  
print $str1;  
?>  

<?php  
$str2 = "Hello!";  
print $str2;  
print "<br>It is Server Side Scripting Language";  
?>  

<?php  
$str3="Hello!";  
$str4="These are the string methods";  
print $str3 . " " . $str4."<br>";  
?>   

<?php  
$age=array("Monica"=>"21");  
print "Monica is " . $age['Monica'] . " years old.<br>";  
?>  

<?php  
$number = 12345;  
printf("%d",$number);
printf("%f",$number);  
?>  