<?php  
$format = 'It is the basic example of PHP String function.';  
$res = sprintf($format);  
echo $res;  
?>  

<?php  
$quantity = 1;  
$language = 'sprintf';  
$format = 'This is the  %dst example of the %s function.';  
$res = sprintf($format, $quantity, $language);  
echo $res;
?>