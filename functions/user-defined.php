<?php
function func1()
{
    echo"Welcome to user-defined functions";
}
func1();
?>

<?php
function func2($arg1, $arg2 = 30)
{
    echo $arg1+$arg2."<br/>";
}
func2(10);
?>

<?php
//conditional functions
$bool = true;
if ($bool) {
    function func3(){
        echo"Welcome to conditional functions";
    }
}
/*if ($bool) {
    func();
}*/
?>

<?php
//function within functions
function func4()
{
    function func5(){
        echo"Inner functions<br/>";
    }
}
func4();
func5();
?>

<?php
function factorial($n){
   if ($n < 2) {
      return 1;
   } else {
      return ($n * factorial($n-1));
   }
}
echo "factorial(5) = ". factorial(5);
?>