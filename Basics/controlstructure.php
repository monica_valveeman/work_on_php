<?php
$a=5;
$b=5;
if ($a > $b) {
    echo "a is bigger than b";
} else if ($a == $b) {
    echo "a is equal to b";
} else {
    echo "a is smaller than b";
}
if ($a > $b):
    echo $a." is greater than ".$b;
elseif ($a == $b): // Note the combination of the words.
    echo $a." equals ".$b;
else:
    echo $a." is neither greater than or equal to ".$b;
endif;

?>

<?php
//while
$sum=0;
$r=0;
$n=1237;
while($n>0){
    $r=$n%10;
    $sum=$sum+$r;
    $n/=10;
}
echo"Sum of digits = $sum";
?>
<?php
//for,foreach
for($i=0;$i<10;$i++){
    echo $i;
}
for($i=0;$i<5;$i++){
    echo"<br/>";
    for($j=$i;$j<3;$j++){
        echo "*";
    }
    echo"<br/>";
}
    $season = array ("Summer", "Winter", "Autumn", "Rainy");  
       
    foreach ($season as $element) {  
        echo "$element";  
        echo "</br>";  
    }   
    $employee = array (  
        "Name" => "Monica",  
        "Email" => "monica1@gmail.com",  
        "Age" => 21,  
        "Gender" => "Female"  
    );  
      
    foreach ($employee as $key => $value) {  
        echo $key . " : " . $value;  
        echo "</br>";   
    }  

?>
<?php
//break and continue
for($x=1;$x<10;$x++){
if($x==5){
        break;
    }
    echo"$x<br/>";
    
}
for($y=1;$y<10;$y++){
    if(!($y%2==0)){
        continue;
    }
    echo"$y<br/>";
}
?>
<?php
include('form.html');
?>
<?php
require_once('form.html');
?>
<?php
include_once('form.html');
?>
<?php
$z=0;
label:
$z++;
echo "$z ";
if($z<10){
    goto label;
}
?>