<?php
$str1="Welcome to string methods";
echo "The string".$str1."<br>";
echo sha1($str1,true)."<br>";
echo sha1($str1)."<br>";
?>

<?php
$str = "Hello";
echo "The string: ".$str."<br>";
echo "TRUE - Raw 20 character binary format: ".sha1($str, TRUE)."<br>";
echo "FALSE - 40 character hex number: ".sha1($str)."<br>";
?>