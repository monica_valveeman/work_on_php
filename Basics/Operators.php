<?php
     $a = 42;
     $b = 20;
      //arithmetic operators      
     $c = $a + $b;
     echo "Addtion : $c <br/>";
            
      $c = $a - $b;
      echo "Substraction : $c <br/>";
            
      $c = $a * $b;
      echo "Multiplication : $c <br/>";
            
      $c = $a / $b;
      echo "Division : $c <br/>";
            
      $c = $a % $b;
      echo "Modulus: $c <br/>";
    ?>
    <?php
    //assignment operators
    $x=10;
    $y=5;
    $z=$a+$b;
    $z+=$x;
    echo "Additiion : $z <br/>";
    $z-=$x;
    echo "Subtration : $z <br/>";
    $z*=$x;
    echo "Multiplicatiion : $z <br/>";
    $z/=$x;
    echo "Division : $z <br/>";
    $z%=$x;
    echo "Modulo : $z <br/>";
    ?>
    <?php
    //increment & decrement operators
    $incdec=5;
    echo "Pre-increment:".++$incdec ."<br/>"; 
    $incdec=5;        
    echo "Post-increment:".$incdec++."<br/>";           
    $incdec=5;
    echo"Pre-decrement:" .--$incdec."<br/>";
    $incdec=5;                                      
    echo "Post-decrement:".$incdec--."<br/>";
    ?>
    <?php
         $a = 42;
         $b = 0;
         
         if( $a && $b ) {
            echo "true<br/>";
         }
         else{
            echo "false<br/>";
         }
         
         if( $a and $b ) {
            echo "true<br/>";
         }
         else{
            echo "false<br/>";
         }
         
         if( $a || $b ) {
            echo "true<br/>";
         }
         else{
            echo "false<br/>";
         }
         
         if( $a or $b ) {
            echo "true<br/>";
         }
         else {
            echo "false<br/>";
         }
         
         $a = 10;
         $b = 20;
         
         if( $a ) {
            echo "true <br/>";
         }
         else {
            echo "false<br/>";
         }
         
         if( $b ) {
            echo "true <br/>";
         }
         else {
            echo "false<br/>";
         }
         
         if( !$a ) {
            echo "true <br/>";
         }
         else {
            echo "false<br/>";
         }
         
         if( !$b ) {
            echo "true <br/>";
         }
         else {
            echo "false<br/>";
         }
      ?>
       <?php
       //comparison operators
         $a = 42;
         $b = 20;
      
         if( $a == $b ) {
            echo "a is equal to b<br/>";
         }
         else {
            echo "a is not equal to b<br/>";
         }
      
         if( $a > $b ) {
            echo"a is greater than  b<br/>";
         }
         else {
            echo"a is not greater than b<br/>";
         }
      
         if( $a < $b ) {
            echo "a is less than  b<br/>";
         }
         else {
            echo "a is not less than b<br/>";
         }
      
         if( $a != $b ) {
            echo "a is not equal to b<br/>";
         }
         else {
            echo "a is equal to b<br/>";
         }
      
         if( $a >= $b ) {
            echo "a is either greater than or equal to b<br/>";
         }
         else {
            echo "a is neither greater than nor equal to b<br/>";
         }
      
         if( $a <= $b ) {
            echo "a is either less than or equal to b<br/>";
         }
         else {
            echo "a is neither less than nor equal to b<br/>";
         }
      ?>
      <?php
      //error control operators
      $fp=@fopen("nosuchfile.txt","r");
      echo "Hello World \n";
      ?>

      <?php
      $list=`dir *.php`;
      echo "$list";
      ?>

      <?php
      //string operator
      $str='Monica';
      $str1=' ';
      $str2='Valveeman';
      $res=$str.$str1.$str2;
      echo"$res";
      ?>
      <?php
      //array operator
         $ar1=array("phy"=>70, "che"=>80, "math"=>90);
         $ar2=array("Eng"=>70, "Bio"=>80,"CompSci"=>90);
         $ar3=$ar1+$ar2;
         var_dump($ar3);
         $arr1=array(0=>70, 2=>80, 1=>90);
         $arr2=array(70,90,80);
         var_dump ($arr1==$arr2);
         var_dump ($arr2!=$arr1);
         var_dump($arr1===$arr2);
      ?>

         <?php
         class testclass{
            
         }
         $a=new testclass();
         if ($a instanceof testclass==TRUE){
            echo '$a is an object of testclass';
         } else {
            echo '$a is not an object of testclass';
         }
         ?>
         <?php
         //checks whether a variable is object of parent class
            class base{
               
            }
            class testclass1 extends base {
               
            }
            $a=new testclass1();
            var_dump($a instanceof base)
            ?>         
           <?php
           //check a variable is a object of interface
            interface base1{
            }
            class testclass2 implements base1 {
               
            }
            $a=new testclass2();
            var_dump($a instanceof base1)
            ?>