<?php
class address{
   var $city="Chennai";
   function setaddr($arg1){
      $this->city=$arg1;
   }
}
class myclass{
   var $name="Monica";
   var $obj;
   function setname($arg){
      $this->name=$arg;
   }
}
$obj1=new myclass();
$obj1->obj=new address();
echo "original object:<br>";
print_r($obj1);
$obj2=clone $obj1;
$obj1->setname("Keerthi");
$obj1->obj->setaddr("Mumbai");
echo "after change:<br>";
echo "original object:<br>";
print_r($obj1);
//doesn't reflect in cloned object
echo "cloned object:<br>";
print_r($obj2);
?>