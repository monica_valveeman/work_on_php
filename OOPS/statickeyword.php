<?php
class staticdemo1 {
    public static function staticmethod() {
        echo"Static method<br>";
    }
}

staticdemo1::staticmethod();
$instance = 'staticdemo1';
$instance::staticmethod();
?>

<?php
//using static variable in a functions
function add() {
  static $number = 0;
  $number++;
  return $number;
}

echo add();
echo "<br>";
echo add();
echo "<br>";
echo add();
?>