<?php
class Methodoverload {
      
      
      public function __call($name, $arguments) {
            
          echo "Calling object method '$name' "
              . implode(', ', $arguments). "<br>";
      }
    
        
      public static function __callStatic($name, $arguments) {
            
          echo "Calling static method '$name' "
              . implode(', ', $arguments). "<br>";
      }
  }
    
  // Create new object
  $obj = new Methodoverload;
    
  $obj->methodss('in object context');
    
  Methodoverload::methodss('in static context'); 
    
  ?>