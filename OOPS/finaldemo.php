<?php
//class using final
final class MyClass {
  public $name = "Monica";
}

// This code will throw an error
//class AnotherClass extends MyClass
//{}
?>

<?php
//method declaration using final
class finaldemo
{
    public function method1() {
        echo"method1";
    }
    final public function method2() { 
        echo"method2";
    }
}
class child extends finaldemo
{
     public function method2() {
        echo"Method3";
    }
}
?>