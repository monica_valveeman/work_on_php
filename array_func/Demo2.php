<?php
//array_flip(array);

$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
print_r(array_flip($a1));
echo"<br>";
?>

<?php
//array_key_exists(key,array);
$a=array("Volvo"=>"XC90","BMW"=>"X5");
if (array_key_exists("Volvo",$a))
  {
  echo "Key exists!";
  }
else
  {
  echo "Key does not exist!";
  }
  echo"<br>";
?>
<?php
//array_keys-returns a array containing keys
$keys=array("Volvo"=>"XC90","BMW"=>"X5");
print_r(array_keys($keys));
echo"<br>";
print_r(array_keys($keys,"X5"));
echo"<br>";
?>

<?php
//array_map
function myfunction($num)
{
  return($num*$num);
}

$map=array(1,2,3,4,5);
print_r(array_map("myfunction",$map));
echo"<br>";
?>
<?php
$m1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
$m2=array("Volvo"=>"XC90","BMW"=>"X5");
print_r(array_merge($m1,$m2));
echo"<br>";
?>
<?php
$m3=array("a"=>"red","b"=>"green");
$m4=array("c"=>"blue","b"=>"yellow");
print_r(array_merge_recursive($m3,$m4));
echo"<br>";
?>

<?php
//multisort
$sort1=array("Dog","Dog","Cat");
$sort2=array("Pluto","Fido","Missy");
array_multisort($sort1,SORT_ASC,$sort2,SORT_DESC);
print_r($sort1);
echo"<br>";
print_r($sort2);
echo"<br>";
?>

<?php
//array_pad(array,size,value);
$pad=array("red","green");
print_r(array_pad($pad,-5,"blue"));
echo "<br>";
print_r(array_pad($pad,5,"blue"));
echo"<br>";
?>

<?php
//array_product(arr);
$a=array(5,5,2,10);
echo(array_product($a));
echo"<br>";
?>

<?php
//reduce
function myfun($v1,$v2)
{
    return $v1+$v2;
}
$red=array(10,15,20);
print_r(array_reduce($red,"myfun",5));
?>
