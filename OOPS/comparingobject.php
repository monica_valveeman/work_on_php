<?php
class test1{
   private $x;
   private $y;
   function __construct($a, $b){
      $this->x=$a;
      $this->y=$b;
   }
}
class test2{
   private $x;
   private $y;
   function __construct($a, $b){
      $this->x=$a;
      $this->y=$b;
   }
}
//two objects are same
$obj1=new test1(10,20);
$obj2=new test1(10,20);
echo "two objects of same class\n";
echo "using == operator : ";
var_dump($obj1==$obj2);
echo "using === operator : ";
var_dump($obj1===$obj2);

//two reference of same object
$reference=$obj1;
echo "two references of same object\n";
echo "using == operator : ";
var_dump($obj1==$reference);
echo "using === operator : ";
var_dump($obj1===$reference);
//two object of different classes
$different=new test2(10,20);
echo "two objects of different classes\n";
echo "using == operator : ";
var_dump($obj1==$different);
echo "using === operator : ";
var_dump($obj1===$different);
?>

