<html>
    <body>
<?php
//$GLOBALS
function func1(){
    $variable1=15;
    echo "The global variable is ".$GLOBALS["variable1"]."<br/>";
    echo "The current variable is $variable1<br/>";
}
$variable1=10;
func1();
?>

<?php
//$_SERVER
echo $_SERVER['PHP_SELF'];
echo "<br>";
echo $_SERVER['SERVER_NAME'];
echo "<br>";
echo $_SERVER['HTTP_HOST'];
echo "<br>";
echo $_SERVER['HTTP_REFERER'];
echo "<br>";
echo $_SERVER['HTTP_USER_AGENT'];
echo "<br>";
echo $_SERVER['SCRIPT_NAME'];
echo"<br/><br/>";
?>


<a href="getform.html">Go to GET method example</a><br/>

<a href="postform.html">Go to POST method example</a><br/>

<a href="fileform.html">Go to FILES method example</a><br/>
</body>
</html>