<?php
//strstr(string,search,bool)
echo strstr("Hello","H")."<br>";
echo strstr("Hello","H",true)."<br>";
?>

<?php
//strpos(string,find,start);
$str1="Welcome to string method";
echo strpos($str1,"str")."<br>";
?>

<?php
//strrev(string);
echo strrev("MONICA")."<br>";
?>

<?php  
    $str1 = "Hi everyone! Welcome to string methods";  
    $del =  "e";  
    //first call of strtok() function  
    $token = strtok($str1, $del);  
    //first call of strtok() function  
    while($token !==false)   
    {  
        echo $token. "<br>";  
        $token =strtok($del);  
    }  
?>

<?php
//substr(string,start,len);
echo substr("Hello world",1,2)."<br>";
echo substr("Hello world",3)."<br>";
echo substr("Hello world",7)."<br>";

echo substr("Hello world",-1)."<br>";
echo substr("Hello world",-10)."<br>";
?>
<?php
echo substr_compare("world","or",1,2);
echo substr_compare("world","ld",-2,2);
echo substr_compare("world","orl",1,2);
echo substr_compare("world","OR",1,2,TRUE)."<br>";
?>

<?php
//ucfirst(string);
echo ucfirst("welcome to php")."<br>";

//ucwords(string);
echo ucwords("welcome to php")."<br>";
?>

<?php
//wordwrap
$str2="This function wraps a string into new lines when it reaches a specific length.";
echo wordwrap($str2,10,"<br>")."<br>";
?>

