<?php
$new = htmlspecialchars("<a href='test'>Test</a>", ENT_QUOTES);
echo $new; // &lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;
?>

<?php  
    $str1= "This is 'PHP' & 'Java' program.";  
    echo htmlspecialchars($str1);
    echo "</br>";  
    echo htmlspecialchars($str1); 
    echo "</br>";  
    echo htmlspecialchars($str1);  
?>  

<?php
//specialchars decode
$str = "This is some &lt;b&gt;bold&lt;/b&gt; text.";
echo htmlspecialchars_decode($str);
?>
<?php
$str = 'I love &quot;PHP&quot;.';
echo htmlspecialchars_decode($str);
?>