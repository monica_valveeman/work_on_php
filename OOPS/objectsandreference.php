<?php
//objects and references

class referencedemo
{
    public $name="Monica";
}

//copying the object one variable to another variable

$object1=new referencedemo();
echo $object1->name ."<br>";
$object2=$object1;
echo $object2->name ."<br>";

//reference the object of another variable
$reference=&$object1;
echo $reference->name."<br>";
$object1->name="Moni";
echo $reference->name."<br>";

//object is sent to arguments
function arg($argobj) {
    $argobj->name="Keerthi";
}
arg($object1);
echo $object1->name;
