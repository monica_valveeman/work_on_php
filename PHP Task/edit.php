<html>
<head>
    <title>Edit a Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="design1.css">
</head>

    <body>
        <?php

        $con=mysql_connect("localhost","root","");
        if(!$con){
            die("Could not connect".mysql_error());
        }
        mysql_select_db("employeereport",$con);

        $id=$_GET['id'];
        $qry = mysql_query("select * from employee where id='$id'",$con);
        $data = mysql_fetch_array($qry); 
        if(isset($_POST['update'])) // when click on Update button
        {
            $name=$_POST['empname'];
            $gender=$_POST['gender'];
            $age=$_POST['empage'];
            $email=$_POST['empmail'];
            $mobile=$_POST['empmobile'];
            $salary=$_POST['empsalary'];

            $sql="update employee set name='$name',gender='$gender',age=$age,email='$email',mobile=$mobile,salary=$salary where id=$id";

            $res=mysql_query($sql,$con);

            if(!$res){
                die("No records are edited:".mysql_error());
            }
            echo"Records edited Successfully";
            mysql_close($con);
        }
        ?>
    <center>
        <br><br>
   
        <div class="addforms">
            <div class="header">
                <h3>Edit a Record</h3>
            </div>
            <form method="POST">
                <table>
                    <tr>
                        <td align="right"> <label>Employee Id: </label></td>
                        <td align="left"><input type="number"  name="empid" id="empid" value="<?php echo $data['id']?>"/></td>
                        <td><span id="idspan"></span></td><br>
                    </tr>
                    <tr>
                        <td align="right"><label>Employee Name: </label></td>
                        <td align="left"><input type="text" name="empname" id="empname" value="<?php echo $data['name']?>" /></td>
                        <td><span id="namespan"></span></td>
                    </tr>
                    <tr>
                        <td align="right"><label>Gender: </label></td>
                        <td align="left">
                            <input type="radio" name="gender" value="Others" id="gen">Others   
                            <input type="radio" name="gender" value="Male" id="gen">Male
                            <input type="radio" name="gender" value="Female" id="gen">Female
                           
                            
                        </td>
                        <td><span id="genderspan"></span></td>
                    </tr>
                    <tr>
                        <td align="right"><label>Age: </label></td>
                        <td align="left"><input type="number" name="empage" id="empage" value="<?php echo $data['age']?>" /></td>
                        <td><span id="agespan"></span></td>
                    </tr>
                    <tr>
                        <td align="right"><label>E-Mail: </label></td>
                        <td align="left"><input type="text" name="empmail" id="empmail" value="<?php echo $data['email']?>"/></td>
                        <td><span id="mailspan"></span></td>
                    </tr>
                    <tr>
                        <td align="right"><label>Mobile No: </label></td>
                        <td align="left"><input type="number" name="empmobile" id="empmobile" value="<?php echo $data['mobile']?>" /></td>
                        <td><span id="mobilespan"></span></td>
                    </tr>
                    <tr>
                        <td align="right"><label>Salary: </label></td>
                        <td align="left"><input type="number" name="empsalary" id="empsalary" value="<?php echo $data['salary']?>" /></td>
                        <td><span id="salspan"></span></td>
                    </tr>
                    <tr>
                        <td></td><td></td>
                        <td><input type="submit" name="update" class="btn btn-success" value="Edit a Record"/></td>
                    </tr>
                </table>
            </form>
        </div>
    </center>
    </body>
</html>

<script>
    let id = document.getElementById('empid');
    id.addEventListener('input', () => {
        if (id.value == '') {
            document.getElementById('idspan').textContent = "*required";
            document.getElementById('idspan').style.display = 'block';
            return false;
        }
        else {
            document.getElementById('idspan').style.display = 'none';
        }
    });

    let name = document.getElementById('empname');
    name.addEventListener('input', () => {
        if (name.value == '') {
            document.getElementById('namespan').textContent = "*required";
            document.getElementBname('namespan').style.display = 'block';
        }
        else if (!(/^[a-z\s]+$/i.test(name.value))) {
            document.getElementById('namespan').textContent = '*only alphabets';
            document.getElementById('namespan').style.display = 'block';
        }
        else {
            document.getElementById('namespan').style.display = 'none';
        }
    });
    let gender = document.getElementById('gen');
    gender.addEventListener('input', () => {
        if (gender.value = '') {
            document.getElementById('genderspan').textContent = '*required';
            document.getElementById('genderspan').style.display = 'block';
        }
        else {
            document.getElementById('genderspan').style.display = 'none';
        }
    });
    let age = document.getElementById('empage');
    age.addEventListener('input', () => {
        if (age.value == '') {
            document.getElementById('agespan').textContent = '*required';
            document.getElementById('agespan').style.display = 'block';
        }
        else {
            document.getElementById('agespan').style.display = 'none';
        }
    });
    let email = document.getElementById('empmail');
    email.addEventListener('input', () => {
        if (email.value == '') {
            document.getElementById('mailspan').textContent = '*required';
            document.getElementById('mailspan').style.display = 'block';
        }
        else if (!(/\w+@\w+\.com/.test(email.value))) {
            document.getElementById('mailspan').textContent = '*invalid mail';
            document.getElementById('mailspan').style.display = 'block';
        }
        else {
            document.getElementById('mailspan').style.display = 'none';
        }
    });

    let mobile = document.getElementById('empmobile');
    mobile.addEventListener('input', () => {
        if (mobile.value == '') {
            document.getElementById('mobilespan').textContent = '*required';
            document.getElementById('mobilespan').style.display = 'block';
        }
        else if (!(/^\d{10}$/.test(mobile.value))) {
            document.getElementById('mobilespan').textContent = '*invalid';
            document.getElementById('mobilespan').style.display = 'block';
        }
        else {
            document.getElementById('mobilespan').style.display = 'none';
        }
    });
    let salary = document.getElementById('empsalary');
    salary.addEventListener('input', () => {
        if (salary.value == '') {
            document.getElementById('salspan').textContent = '*required';
            document.getElementById('salspan').style.display = 'block';
        }
        else {
            document.getElementById('salspan').style.display = 'none';
        }
    });

</script>