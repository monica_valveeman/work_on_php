<?php

//basename()

$path="C:\wamp\www\Basics\datatypes.php";

echo basename($path)."<br>";

echo basename($path,".php")."<br>";
?>

<?php
//chmod
   // Read and write for owner, nothing for everybody else
   chmod("C:\wamp\www\File Systems\permissions.txt", 0600);

   // Read and write for owner, read for everybody else
   chmod("C:\wamp\www\File Systems\permissions.txt", 0644);

   // Everything for owner, read and execute for everybody else
   chmod("C:\wamp\www\File Systems\permissions.txt", 0755);

   // Everything for owner, read for owner's group
   chmod("C:\wamp\www\File Systems\permissions.txt", 0740);
?>

<?php
//copy
$copy_from="file1.txt";
$copy_to="file2.txt";

if(!copy($copy_from,$copy_to)){
    echo"Failed to copy a file";
}
?>

<?php
//dirname
echo dirname("C:\wamp\www\Basics\datatypes.php")."<br>";

?>